<?php

namespace Interfaces;

interface EmployeeInterface
{
    public function prepareName(): self;

    public function nameWithAge(): string;
}