<?php
require_once 'Autoload.php';

use Classes\ProgrammerClass;

$programmer1 = new ProgrammerClass( 'nataliia',19, 'php', 2000);

echo $programmer1
    ->prepareName() // class
    ->nameWithAge(); // string

echo "</br>";

echo $programmer1
    ->prepareLang()
    ->langAndSalary();


