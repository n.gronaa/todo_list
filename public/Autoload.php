<?php
spl_autoload_register(function ($className){

    $fileLink = str_replace('\\', '/', $className);
    $fileLink = __DIR__ . '/' .$fileLink . '.php';

    if (file_exists($fileLink)) {
        require $fileLink;
    }
});




