<?php
declare(strict_types=1);

namespace Classes;

use Interfaces\EmployeeInterface;

class ProgrammerClass extends EmployeeClass implements EmployeeInterface
//class ProgrammerClass extends EmployeeClass
{
    public function __construct($name, $age, public ?string $lang = '', public ?int $salary = 0)
    {
        parent::__construct($name, $age);
    }


    public function prepareLang(): self
    {
        $this->lang = strtoupper($this->lang);

        return $this;
    }


    public function langAndSalary(): string
    {
        return "I`m $this->lang developer and my salary is $this->salary";
    }

}