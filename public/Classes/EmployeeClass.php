<?php
declare(strict_types=1);

namespace Classes;

use Interfaces\EmployeeInterface;

class EmployeeClass implements EmployeeInterface
//class EmployeeClass
{
    public function __construct(public ?string $name = '', public int|null $age = null)
    {
        //
    }

    /**
     * @return self
     */
    public function prepareName(): self
    {
        if (is_null($this->name)) {
            die('Name not specified');
        }

        $this->name = ucfirst($this->name);

        return $this;
    }

    /**
     * @return string
     */
    public function nameWithAge(): string
    {
        return "Name is $this->name and age is $this->age.";
    }
}



//    public function __SET($name, $value)
//    {
//        $this->$name = $value;
//    }
//
//    public function __GET($name)
//    {
//        return 'value not exists';
//    }